import pandas as pd

data = {
  "calories": [700, 700, 800],
  "duration": [55, 52, 1]
}

#load data into a DataFrame object:
df = pd.DataFrame(data)

print(df)
df.to_csv('test.csv')
